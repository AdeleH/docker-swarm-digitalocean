#!/bin/bash

# Idea from
# https://blog.codeship.com/nginx-reverse-proxy-docker-swarm-clusters/

# Tutorial
# https://www.digitalocean.com/community/tutorials/how-to-create-a-cluster-of-docker-containers-with-docker-swarm-and-digitalocean-on-centos-7

####
# Configuration
####

# Number of nodes
# A cluster has to have at least one node that serves as a manager,
# though for a production setup, three managers are recommended.
NB_MANAGERS=3
NB_WORKERS=3

OS_NODES=ubuntu-16-04-x64
DROPLET_REGION="fra1"  # default: nyc3

MANAGER_NODE_SIZE="1gb"
WORKER_NODE_SIZE="512mb"

MANAGER_NODE_NAME=manager
WORKER_NODE_NAME=worker

# $DIGITALOCEAN_ACCESS_TOKEN ===========> see env.sh

# DO Speed Test per Region:
# http://speedtest-ams2.digitalocean.com/

# Swarm Strategy -> default: spread
# https://github.com/docker/docker.github.io/blob/master/swarm/scheduler/strategy.md
# Using the spread strategy results in containers spread thinly over many machines.

#####
## Step 1 - Create cluster of nodes
#####

create_manager() {
    echo "Creating manager node $MANAGER_NODE_NAME-$i..."

    # Manager nodes are called 'manager-0', 'manager-1', ...
    docker-machine create \
    --driver digitalocean \
    --digitalocean-size "$MANAGER_NODE_SIZE" \
    --digitalocean-region "$DROPLET_REGION" \
    --digitalocean-ipv6 \
    --digitalocean-backups \
    --digitalocean-image $OS_NODES \
    --swarm --swarm-master \
    --digitalocean-access-token $DIGITALOCEAN_ACCESS_TOKEN $MANAGER_NODE_NAME-$i
    wait

    echo "Done."
}

create_worker() {
    echo "Creating worker node $WORKER_NODE_NAME-$i..."

    # Worker nodes are called 'worker-0', 'worker-1', ...
    docker-machine create \
    --driver digitalocean \
    --digitalocean-size "$WORKER_NODE_SIZE" \
    --digitalocean-region "$DROPLET_REGION" \
    --digitalocean-ipv6 \
    --digitalocean-backups \
    --digitalocean-image $OS_NODES \
    --swarm \
    --digitalocean-access-token $DIGITALOCEAN_ACCESS_TOKEN $WORKER_NODE_NAME-$i
    wait

    echo "Done."
}

create_cluster() {
    echo "\n"
    echo "***Create cluster***"
    echo "\n"

    for ((i=0; i<$NB_MANAGERS; i++)); do create_manager; done
    for ((i=0; i<NB_WORKERS; i++)); do create_worker; done
}

####
# Step 2 - Configure Firewall
# https://www.digitalocean.com/community/tutorials/how-to-configure-the-linux-firewall-for-docker-swarm-on-centos-7
####

# TCP port 2376 for secure Docker client communication.
# This port is required for Docker Machine to work.
# Docker Machine is used to orchestrate Docker hosts.

# TCP and UDP port 7946 for communication among nodes (container network discovery).

# UDP port 4789 for overlay network traffic (container ingress networking).

# TCP port 2377.
# This port is used for communication between the nodes of a Docker Swarm or cluster.
# It only needs to be opened on manager nodes.

# ==>
# firewall-cmd --add-port=2376/tcp --permanent  #  ALL = MANAGER AND WORKER
# firewall-cmd --add-port=7946/tcp --permanent  #  ALL
# firewall-cmd --add-port=7946/udp --permanent  #  ALL
# firewall-cmd --add-port=4789/udp --permanent  #  ALL
# firewall-cmd --add-port=2377/tcp --permanent  #  MANAGER ONLY

configure_node_firewall() {
    node_name=$1
    is_manager=$2
    echo "node_name: $node_name"
    echo "is_manager: $is_manager"

    echo "Configuring Firewall for $node_name..."

    # Install UFW
    # UFW on Ubuntu: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-16-04
    install_UFW_command="sudo apt update -y && sudo apt-get install ufw -y && sudo apt-get clean -y"

    echo "install_UFW_command: $install_UFW_command"
    docker-machine ssh $node_name "$install_UFW_command"
    wait

    # Basic configuration for worker nodes
    configure_UFW_command="sudo ufw default deny incoming"
    configure_UFW_command="$configure_UFW_command && sudo ufw default allow outgoing"
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 2376/tcp"  # rule #1
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 7946/tcp"  # rule #2
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 7946/udp"  # rule #3
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 4789/udp"  # rule #4

    # Allow SSH (20)
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 22"

    # Allow HTTP (80) and HTTPS (443)
    configure_UFW_command="$configure_UFW_command && sudo ufw allow 80 && sudo ufw allow 443"

    # Manager Only
    if $is_manager
    then
        configure_UFW_command="$configure_UFW_command && sudo ufw allow 2377/tcp"  # rule #5
    fi

    configure_UFW_command="$configure_UFW_command && sudo ufw --force enable && sudo ufw status"

    echo "configure_UFW_command: $configure_UFW_command"
    docker-machine ssh $node_name "$configure_UFW_command"
    wait

    echo "Done."
}

configure_firewall() {
    echo "\n"
    echo "***Configure Firewall***"
    echo "\n"

    for ((i=0; i<$NB_MANAGERS; i++)); do configure_node_firewall $MANAGER_NODE_NAME-$i True; done
    for ((i=0; i<$NB_WORKERS; i++)); do configure_node_firewall $WORKER_NODE_NAME-$i False; done
}

####
# Step 3 — Initializing The Cluster Manager
#####

join_swarm_cluster() {
    node_name=$1
    ip=$2
    token=$3

    echo "node_name: $node_name"
    echo "ip: $ip"
    echo "token: $token"

    echo "$node_name joining the cluster..."

    docker-machine ssh $node_name "docker swarm join --token $token $ip:2377"

    echo "Done"
}

configure_swarm_cluster() {
    echo "\n"
    echo "***Configure Swarm Cluster***"
    echo "\n"

    # Fetch IP
    #ip=`docker-machine ls --filter driver=digitalocean  --format "{{.Name}} => {{.URL}}" | grep $MANAGER_NODE_NAME-0 | egrep -o "\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}"`
    ip=`docker-machine ip $MANAGER_NODE_NAME-0`

    echo "Init Swarm Cluster"
    docker-machine ssh $MANAGER_NODE_NAME-0 "docker swarm init --advertise-addr $ip"

    # Fetch Tokens
    # https://gist.github.com/twang2218/bf84e0c1e1962cc183f17ea684b0b9d9
    ManagerToken=`docker-machine ssh $MANAGER_NODE_NAME-0 "docker swarm join-token manager | grep token" | awk '{ print $5 }'`
    WorkerToken=`docker-machine ssh $MANAGER_NODE_NAME-0 "docker swarm join-token worker | grep token" | awk '{ print $5 }'`

    for ((i=1; i<$NB_MANAGERS; i++)); do join_swarm_cluster $MANAGER_NODE_NAME-$i $ip $ManagerToken; done
    for ((i=0; i<NB_WORKERS; i++)); do join_swarm_cluster $WORKER_NODE_NAME-$i $ip $WorkerToken; done

}

show_cluster() {
    echo "\n"
    echo "***Show cluster***"
    echo "\n"

    docker-machine ssh $MANAGER_NODE_NAME-0 "docker node ls"
}
####
# Step 4 - If needed - Delete cluster
####

delete_manager() {
    echo "Deleting manager node $MANAGER_NODE_NAME-$i..."

    docker-machine rm $MANAGER_NODE_NAME-$i -y
    wait

    echo "Done."
}

delete_worker() {
    echo "Deleting worker node $WORKER_NODE_NAME-$i..."

    docker-machine rm $WORKER_NODE_NAME-$i --force -y
    wait

    echo "Done."
}

show_machine() {
    echo "\n"
    echo "***Show machines***"
    echo "\n"

    docker-machine ls
}

delete_cluster() {
    echo "\n"
    echo "***Delete cluster***"
    echo "\n"

    for ((i=0; i<$NB_MANAGERS; i++)); do delete_manager; done
    for ((i=0; i<NB_WORKERS; i++)); do delete_worker; done
}

####
# Update packages - If needed
####

update_cluster_packages() {
    echo "\n"
    echo "***Update packages***"
    echo "\n"

    command="sudo apt update -y && sudo apt-get clean -y"

    for ((i=0; i<$NB_MANAGERS; i++)); do docker-machine ssh $MANAGER_NODE_NAME-$i "$command"; done
    for ((i=0; i<$NB_WORKERS; i++)); do docker-machine ssh $WORKER_NODE_NAME-$i "$command"; done
}

####
# Resume
# $> time sh create_swarm_cluster_on_DO.sh
#  15' ~ for 3 managers and 3 nodes (each machine creation itself takes few minutes)
####

# Creation and configuration (FW) of the nodes
create_cluster
configure_firewall

# Build and configure the cluster
configure_swarm_cluster
update_cluster_packages
show_cluster

# Delete the cluster
delete_cluster
show_machine