# Docker Swarm using Digitalocean

A script to create and configure a cluster of nodes (Docker Swarm) in few minutes:

- create the droplet takes few minutes

- configure the firewall (UFW)

- join the manager and worker nodes into a cluster

For 3 managers and 3 workers it takes appr. 15 minutes.

![Alt text](/screenshots/DO_droplets_overview.png?raw=true "Overview of the droplets on DigitalOcean")


## Run the script 

- Create Digitalocean access token on https://cloud.digitalocean.com/settings/api/tokens and export variable value

```
$> export $DIGITALOCEAN_ACCESS_TOKEN=<digitalocean-api-token>
```

- Launch the script

```
$> sh create_swarm_cluster_on_DO.sh
```

![Alt text](/screenshots/cluster_overview.png?raw=true "Overview of the cluster")

- **Costs** of the operation

The creation of one node costs **$0.01**.

So for 3 workers and 3 managers, the operation costs $0.06


### More info about the command line parameters

```
$> docker-machine create --driver digitalocean -h
```

